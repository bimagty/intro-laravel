<!DOCTYPE html>
<html>
<head>
	<title> Sign Up </title>
	<meta charset="UTF-8">
</head>	

<body>
	<h2> Buat Account Baru! </h2>
	<h3> Sign Up Form </h3>
	<form action="/post" method="post">
    @csrf
		<fieldset>
			<label for="firstname"> First name: </label>
			<input type="text" name="namadepan" placeholder="Your first name.." id="firstname"> <br>
			<label for="lastname"> Last name: </label>
			<input type="text" name="namabelakang" placeholder="Your last name.." id="lastname"> <br><br>
			
			<label for="gender"> Gender: </label> <br>
			<input type="radio" name="gender_user" value="0"> Male <br>
			<input type="radio" name="gender_user" value="1"> Female <br>
			<input type="radio" name="gender_user" value="0"> Other <br><br>
			<label for="nationality"> Nationality: </label><br>
			<select>
				<option value="indo"> Indonesia </option>
				<option value="nonindo"> Non-Indonesia </option>
			</select>
			<br><br>

			<label for="language"> Language spoken: </label> <br>
			<input type="checkbox" name="language_user" value="0"> Bahasa Indonesia <br>
			<input type="checkbox" name="language_user" value="1"> English <br>
			<input type="checkbox" name="language_user" value="0"> Other <br><br>

			<label for="bio"> Bio: </label> <br>
			<textarea cols="20" rows="6" placeholder="Write down your bio.."></textarea> <br><br>

			<input type="submit" value="Sign Up">
		</fieldset>
	</form>

</body>
</html>